
# LIS 4381

## Grace Bayliss

### Assignment #1 Requirements:

1. Distributed version control with Git and BitBucket
2. Development installations
3. Questions
4. BitBucket Repo links:
    a) https://bitbucket.org/gab19d/lis4381/src/master/
    b) https://bitbucket.org/gab19d/bitbucketstationlocations/src/master/

#### README.md file should include the following items:

* screenshot of ampps running
* screenshot of running java hello
* screenshot of running Android Studio - My First App
* git commands with short descriptions
* BitBucket repo links
> 
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing
       one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local
       branch
7. git remote - Manage set of tracked repositories

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/java.png "JDK installation screenshot")

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/androidstudio.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")


