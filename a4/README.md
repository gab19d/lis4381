
# LIS 4381

## Grace Bayliss

### Assignment #4 Requirements:

1. Create local lis4381 app
2. Basic client-side validation
3. Complete skillsets 10-12
4. Questions

#### README.md file should include the following items:

* Screenshot of local lis4381 app home page
* Screenshot of local lis4381 app A4 page passed
* Screenshot of local lis4381 app A4 page failed
* Screenshot of Skill Sets 10-12
* Links to local lis4381 web app:
     a) http://localhost/repos/lis4381/ 

> 
>


#### Assignment Screenshots:

| *Screenshot of lis4381 web app home page*: | *Screenshot of A4 passed*:       |
|--------------------------------------------|----------------------------------|
| ![app running](img/home.png)               | ![app running](img/pass.png)     |

| *A4 failed*:                   |
|![A4 failed](img/fail.png)      |

| *Skill Sets*:                |                              |                                    |
|------------------------------|------------------------------|------------------------------------|
|![Skill set 10](img/ss10.png) |![Skill set 11](img/ss11.png) |![Skill set 12](img/ss12.png)       |




