
# LIS 4381

## Grace Bayliss

### Project #2 Requirements:

1. Server-side Validation
2. Edit/delete in the petstore table
3. Create an RSS feed page
4. Questions

#### README.md file should include the following items:

* Screenshot of lis 4381 index.php page
* Screenshot of P2 index.php page
* Screenshot of edit_petstore.php page
* Screenshot of failed edit_petstore.php page
* Screenshot of passed edit_petstore.php page
* Screenshot of delete prompt
* Screenshot of deleted prompt
* Screenshot of RSS feed page
* Links to local lis4381 web app:
     a) http://localhost/repos/lis4381/ 

> 
>


#### Assignment Screenshots:

| *Screenshot of lis4381 index.php*: | *Screenshot of index.php*:           |
|------------------------------------|----------------------------------    |
| ![app running](img/carousel.png)   | ![app running](img/index.png)        |

*Screenshot of edit_petstore.php*: 
![edit](img/edit.png)

*Screenshot of edit_petstore.php (fail)*: 
![fail](img/fail2.png)

*Screenshot of edit_petstore_process.php (passed validation)*:
![error](img/edit1.png)

*Screenshot of Delete Prompt*:         
![prompt](img/prompt.png)
*Screenshot of Deleted Record*             
![Skill set 14](img/delete.png)                 
*Screenshot of RSS Feed*                             
![Skill set 15](img/rss.png) 



