
# LIS 4381

## Grace Bayliss

### Project #1 Requirements:

1. Create a business card app
2. Complete skill sets 7-9
3. Questions

#### README.md file should include the following items:

* Screenshot of running application's first user interface
* Screenshot of running application's second user interface
* Screenshot of running ss7
* Screenshot of running ss8
* Screenshot of running ss9
> 
>

#### Assignment Screenshots:

| *Screenshot of Business Card running*: | *Screenshot of Business Card running*:  |
|----------------------------------------|-----------------------------------------|
| ![app running](img/app.png)            | ![details running](img/app4.png)        |


| *Skill Sets*:              |                            |                            |
|----------------------------|----------------------------|----------------------------|
|![Skill set 7](img/ss7.png) |![Skill set 8](img/ss8.png) |![Skill set 9](img/ss9.png) |



