<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Mark K. Jowett, Ph.D.">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong>1. Create a mobile recipe app
2. Complete skill sets 1-3
3. Questions</p>

				<h4>Recipe App</h4>
				<img src="img/app.png" class="img-responsive center-block" alt="Recipe">
				<img src="img/app2.png" class="img-responsive center-block" alt="Recipe">

				<h4>Skill Set 1</h4>
				<img src="img/app3.png" class="img-responsive center-block" alt="Skill Set 1">

				<h4>Skill Set 2</h4>
				<img src="img/app4.png" class="img-responsive center-block" alt="Skill Set 2">

				<h4>Skill Set 3</h4>
				<img src="img/app5.png" class="img-responsive center-block" alt="Skill Set 3">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
