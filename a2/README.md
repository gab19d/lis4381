
# LIS 4381

## Grace Bayliss

### Assignment #2 Requirements:

1. Create a mobile recipe app
2. Complete skill sets 1-3
3. Questions

#### README.md file should include the following items:

* Screenshot of running application's first user interface
* Screenshot of running application's second user interface
* Screenshot of running ss1
* Screenshot of running ss2
* Screenshot of running ss3
> 
>

#### Assignment Screenshots:

*Screenshot of Bruschetta Recipe running*:

![Bruschetta Recipe running](img/app.png) ![Bruschetta Recipe running](img/app2.png) 

*Screenshot of running ss1*:

![Skillset 1](img/app3.png)

*Screenshot of running ss2*:

![Skillset 2](img/app4.png)

*Screenshot of running ss3*:

![Skillset 3](img/app5.png)



