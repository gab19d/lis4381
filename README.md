
# LIS 4381 - Mobile Web App Development

## Grace Bayliss

### LIS 4381 Requirements:

*Course Work Links:*

1. [A1_README.md](a1/README.md " My A1 README.md file")
    - Distributed version control with Git and BitBucket
    - Development installations
    - Questions
    - BitBucket Repo links
2. [A2_README.md](a2/README.md "My A2 README.md file")
    - Create a mobile recipe app using Android Studio
    - Complete Skill sets 1-3 
    - Questions

3. [A3_README.md](a3/README.md "My A3 README.md file")
    - Create ERD with 10 records in each table
    - Create mobile app using Android Studio
    - Complete skill sets 4-6
    - Questions
4. [P1_README.md](p1/README.md "My P1 README.md file")
    - Create mobile app using Android studio
    - Complete skill sets 7-9
    - Questions
5. [A4_README.md](a4/README.md "My A4 README.md file")
    - Create local lis4381 web app
    - Basic client-side validation
    - Complete skill sets 10-12
    - Questions
 6. [A5_README.md](a5/README.md "My A5 README.md file")
    - Server-side Validation
    - Update petstore table
    - Complete skillsets 13-15
    - Questions
7. [P2_README.md](p2/README.md "My P2 README.md file")
    - Server-side Validation
    - Edit/Delete in petstore table
    - Create RSS Feed Page
    - Questions