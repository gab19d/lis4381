
# LIS 4381

## Grace Bayliss

### Assignment #5 Requirements:

1. Server-side Validation
2. Update petstore table
3. Complete skillsets 13-15
4. Questions

#### README.md file should include the following items:

* Screenshot of index.php page
* Screenshot of invalid add_petstore.php page
* Screenshot of valid add_petstore.php page
* Screenshot of failed add_petstore_process.php page
* Screenshot of passed add_petstore_process.php page
* Screenshot of Skill Sets 13-15
* Links to local lis4381 web app:
     a) http://localhost/repos/lis4381/ 

> 
>


#### Assignment Screenshots:

| *Screenshot of lis4381 index.php*: | *Screenshot of add_petstore.php (invalid)*: |
|------------------------------------|----------------------------------           |
| ![app running](img/index.png)      | ![app running](img/fail1.png)               |

*Screenshot of add_petstore.php (valid)*: 
![failed](img/valid.png)

*Screenshot of add_petstore.php (failed validation)*: 
![failed](img/error.png)

*Screenshot of add_petstore_process.php (passed validation)*:
![passed](img/passed.png)

*Skill Sets*:   
*13*         
![Skill set 13](img/ss13.png)
*14*             
![Skill set 14](img/ss14.png)        
![Skill set 13](img/ss142.png)            
*15*                             
![Skill set 15](img/ss15.png) 
![Skill set 15](img/ss152.png) 



