<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Mark K. Jowett, Ph.D.">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> 1. Create mobile app
2. Create ERD with 10 records in each table
3. Complete skillsets 4-6
4. Questions</p>

				<h4>Concert App</h4>
				<img src="img/app1.png" class="img-responsive center-block" alt="app">
				<img src="img/app2.png" class="img-responsive center-block" alt="app">

				<h4>ERD</h4>
				<img src="img/erd.png" class="img-responsive center-block" alt="ERD">
				<img src="img/customer.png" class="img-responsive center-block" alt="customer table">
				<img src="img/pet.png" class="img-responsive center-block" alt="pet table">
				<img src="img/pst.png" class="img-responsive center-block" alt="store table">

				<h4>Skill Sets</h4>
				<img src="img/ss4.png" class="img-responsive center-block" alt="Skill Sets">
				<img src="img/ss5.png" class="img-responsive center-block" alt="Skill Sets">
				<img src="img/ss6.png" class="img-responsive center-block" alt="Skill Sets">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
