
# LIS 4381

## Grace Bayliss

### Assignment #3 Requirements:

1. Create mobile app
2. Create ERD with 10 records in each table
3. Complete skillsets 4-6
4. Questions

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of running application's first user interface
* Screenshot of running application's second user interface
* Screenshot of 10 records for each table
* Screenshots of skillsets 4-6
* Links to the following files:
     a) a3.mwb
     b) a3.sql

> 
>


#### Assignment Screenshots:

| *Screenshot of concert app running*: | *Screenshot of app running*: |
|--------------------------------------|------------------------------|
| ![app running](img/app1.png)         | ![app running](img/app2.png) |

| *ERD*:                             |                      |                                    |
|------------------------------------|----------------------|------------------------------------|
|![ERD](img/erd.png)                 |                      |
|![Customer Table](img/customer.png) |![Pet Table](img/pet.png) |![Pet Store Table](img/pst.png) |

| *Skill Sets*:          |                        |                                    |
|------------------------|------------------------|------------------------------------|
|![Skill set 4](img/ss4.png) |![Skill set 5](img/ss5.png) |![Skill set 6](img/ss6.png) |

#### Links:

*A3 mySQL development:*
[A3 .mwb](a3.mwb "a3 mysql development")
[A3 .sql](a3.sql "a3 mysql development")


